
![Logo](https://cdn.techinasia.com/data/images/peV8gGR3ZwETFNCdRAoRcH7ke7vjKS64TB5KHKj2.png)

# Notification History Services

Notification history service is the service responsible for sending notifications to database


## Documentation
- [TRD](https://www.atlassian.com/software/confluence?&aceid=&adposition=&adgroup=99365841496&campaign=9616888294&creative=542596263091&device=c&keyword=confluence&matchtype=e&network=g&placement=&ds_kids=p52353097996&ds_e=GOOGLE&ds_eid=700000001542923&ds_e1=GOOGLE&gclid=Cj0KCQjw4PKTBhD8ARIsAHChzRLpnkQX8tlBmfmKf6IazEhzsMvB59IOFI7Xg41ePqTlxr6_EBUczzcaAoTEEALw_wcB&gclsrc=aw.ds)
- [FDS](https://www.figma.com/file/Y4HBMfpaFMujnjNtGGXxvA/Untitled?node-id=0%3A1)
- [Whimsical](https://www.figma.com/file/Y4HBMfpaFMujnjNtGGXxvA/Untitled?node-id=0%3A1)
- [Last Deployment Document](https://www.atlassian.com/software/confluence?&aceid=&adposition=&adgroup=99365841496&campaign=9616888294&creative=542596263091&device=c&keyword=confluence&matchtype=e&network=g&placement=&ds_kids=p52353097996&ds_e=GOOGLE&ds_eid=700000001542923&ds_e1=GOOGLE&gclid=Cj0KCQjw4PKTBhD8ARIsAHChzRLpnkQX8tlBmfmKf6IazEhzsMvB59IOFI7Xg41ePqTlxr6_EBUczzcaAoTEEALw_wcB&gclsrc=aw.ds)



## Database

- **MongoDB** (collection1, collection2, collection3)
- **PostgreSQL** (table1, table2, table3)
- **QasirDB** (table1, table2, table3)


## Transport

* **RPC** 
    * Notification-service-grpc
    * Nirebase-service-grpc
* **Pubsub** 
    * **Topic**
        * Topic1
        * Topic2
        * Topic3
    * **Sub**
        * Sub1
        * Sub2
        * Sub3

## Related Services

Here are some related services

- [Firebase-Messaging-Service](https://github.com/matiassingers/awesome-readme)
- [Notification-Edge](https://github.com/matiassingers/awesome-readme)
- [Qasir-Pos-Gateway](https://github.com/matiassingers/awesome-readme)


## Installation

Clone the project Master Branch
```bash
  go mod tidy 
  go run main.go
```
    
## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

```bash
API_KEY = lalalalayeye
POS_API = http://pos.com/api/v5

```

## Contributors

- [@leonardo](https://www.github.com/leonardo11)
- [@leo2](https://www.github.com/leonardo11)
- [@leo3](https://www.github.com/leonardo11)

